from fastapi import FastAPI
from fastapi_cache import FastAPICache
from fastapi_cache.backends.redis import RedisBackend
from redis import asyncio as aioredis
from fastapi.responses import HTMLResponse
from starthtml import pagestart
import uvicorn
from dbcon import create_database_if_not_exists, create_shema_for_database
from dbcon.database import datadb, check_database_exists
from router import routesposts, routesusers



app = FastAPI(title='ExampleSocialNetwork',
              description='Наша социальная сеть - это место, где люди могут поделиться своими мыслями и идеями. \n'
                          'Здесь вы найдете множество интересных статей на разные темы: от музыки до науки, от моды до спорта. \n'
                          'А самое главное - вы можете дать оценку своим друзьям за их труды и получать оценки взамен. \n'
                          'В нашей социальной сети вы найдете огромное количество интересных людей, готовых поддержать вас и поделиться своими мыслями.\n')



@app.get("/", response_class=HTMLResponse)
async def start():
    """
    Назначение: \n
    Показать пользователю стартовую страницу со ссылкой на Swagger для использования API.\n
    """
    return pagestart.start_generate_html()


@app.on_event("startup")
async def startup():
    redis = await aioredis.from_url("redis://localhost:6379", encoding="utf8", decode_responses=True)
    backend = RedisBackend(redis)
    FastAPICache.init(RedisBackend(redis), prefix="fastapi-cache")
    result_check_database = await check_database_exists()
    if not result_check_database: await create_database_if_not_exists()
    await create_shema_for_database()
    await datadb.connect()


@app.on_event("shutdown")
async def shutdown():
    backend = FastAPICache.get_backend()
    if isinstance(backend, RedisBackend):
        redis = backend.redis
        await redis.close()
    await datadb.disconnect()


app.include_router(routesposts.routepost)
app.include_router(routesusers.routeuser)

if __name__ == "__main__":
    uvicorn.run("main:app", port=8000, host="0.0.0.0", reload=True, workers=4)

#container_name_or_id = "my-redis-container"
#redis_url = f"redis://{container_name_or_id}:6379"
#redis-cli -h 172.17.0.2 -p 6379

#ae51fb92-e2c8-43fd-993e-d057f54f0ad0
#53465d6a-295b-47ba-a15d-64f08ee3f5cd
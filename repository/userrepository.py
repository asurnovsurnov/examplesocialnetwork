from router import *
from repository.baserepository import BaseRepository

class UserRepository(BaseRepository):

    async def get_users(self, skip: int = 0, limit: int = 100):
        user = models.SocialUser.__table__
        query = user.select().\
            with_only_columns([user.c.id, user.c.first_name, user.c.last_name, user.c.email,
                               user.c.about_me, user.c.register_date]).offset(skip).limit(limit)
        result_list = await self._database.fetch_all(query)
        return [dict(result) for result in result_list]

    async def get_user(self, user_id: str):
        user = models.SocialUser.__table__
        post = models.Post.__table__
        query = user.select().select_from(user.outerjoin(post, user.c.id == post.c.author_id)).where(user.c.id == user_id).\
            column(post.c.id).column(post.c.create_date).column(post.c.update_date)
        try:
            result = await self._database.fetch_all(query)
        except (ValueError, AssertionError):
            raise HTTPException(status_code=400, detail=f'Non-correct data format: {user_id}')
        result = [dict(el) for el in result]
        if not result: raise HTTPException(status_code=404, detail=f'Dont have user with id: {user_id}')
        answer = {}
        for item in result:
            events_list = answer.setdefault('posts', [])
            event_dict = {}
            for key in ['id_1', 'create_date', 'update_date']:
                event_dict[key] = item.pop(key)
            events_list.append(event_dict)
            answer.update(item)
        if len(answer['posts']) == 1 and answer['posts'][0]['id_1'] == None: answer['posts'] = []
        del answer['password']
        return answer

    async def search_users(self, search_string: str, how_search: SearchUser, skip: int = 0, limit: int = 100):
        user = models.SocialUser.__table__
        query = user.select().with_only_columns([user.c.id, user.c.first_name, user.c.last_name, user.c.email, user.c.about_me, user.c.register_date]).\
            where((func.lower(user.c.first_name).contains(func.lower(search_string)))|
                  (func.lower(user.c.last_name).contains(func.lower(search_string)))|
                  (func.lower(user.c.about_me).contains(func.lower(search_string)))).\
            offset(skip).limit(limit)
        if how_search == 'name':
            query = user.select().with_only_columns([user.c.id, user.c.first_name, user.c.last_name, user.c.email, user.c.about_me, user.c.register_date]).\
                where((func.lower(user.c.first_name).contains(func.lower(search_string)))|
                      (func.lower(user.c.last_name).contains(func.lower(search_string)))).\
                offset(skip).limit(limit)
        result_list = await self._database.fetch_all(query)
        if not result_list:
            if (len(search_string)) < 21: return [{'msg': f'Dont have user in system with text: {search_string}'}]
            else: return [{'msg': f'Dont have user in system with text: {search_string[:8]}....{search_string[len(search_string)-10:len(search_string)]}'}]
        return [dict(result) for result in result_list]

    async def make_singup_user(self, user_details: shemas.BaseUser):
        check_query = models.SocialUser.__table__.select().where(
            models.SocialUser.__table__.c.email == user_details.email)
        check_mentor = await datadb.fetch_all(check_query)
        if check_mentor:
            return {'msg': 'Account already exists'}
        try:
            mentor_table = models.SocialUser.__table__
            hashed_password = auth_handler.encode_password(user_details.password)
            user_details.password = hashed_password
            new_user_id = uuid.uuid4().hex
            stmt = mentor_table.insert().values(id=new_user_id, **user_details.dict(exclude_none=True))
            new_user = await self._database.execute(stmt)
            user_info = {'name': user_details.first_name, 'id': new_user_id}
            return user_info
        except Exception as e:
            print(e)
            error_msg = 'Failed to signup user'
            return {'msg': error_msg}

    async def make_login_user(self, user_details: shemas.UserAuth):
        user = models.SocialUser.__table__
        query = user.select().where(user.c.email == user_details.email)
        result = dict(await self._database.fetch_one(query))
        if (result is None) or (not auth_handler.verify_password(user_details.password, result['password'])):
            raise HTTPException(status_code=401, detail='Invalid password or Invalid username')
        access_token = auth_handler.encode_token(str(result['id']))
        # refresh_token = auth_handler.encode_refresh_token(user_details.first_name)
        return {'access_token': access_token}

    async def password_change_user(self, user_details: shemas.UserPasswordChange):
        user = models.SocialUser.__table__
        query = user.select().where((user.c.email == user_details.email) & (user.c.id == user_details.id))
        result = await self._database.fetch_one(query)
        if result is None:
            raise HTTPException(status_code=401, detail='Invalid username')
        result = dict(result)
        hashed_password = auth_handler.encode_password(user_details.new_password)
        user_details.new_password = hashed_password
        stmt = user.update().where(user.c.id == user_details.id).values(password=user_details.new_password).returning(user)
        result = await self._database.fetch_one(stmt)
        if result is None:
            raise HTTPException(status_code=406, detail='Error. Password not change.')
        result = dict(result)
        return {'msg': 'you update password.', 'user_id': result['id']}

    async def name_change_user(self, user_details: shemas.UserNameChange, id_user: str):
        user = models.SocialUser.__table__
        stmt = user.update().where(user.c.id == id_user).values(first_name=user_details.first_name, last_name=user_details.last_name).returning(user)
        result = await self._database.fetch_one(stmt)
        if result is None:
            raise HTTPException(status_code=406, detail='Error. Name not change.')
        return {'msg': 'you update name.', 'user_id': id_user}

    async def about_change_user(self, user_details: shemas.UserAboutChange, id_user: str):
        user = models.SocialUser.__table__
        stmt = user.update().where(user.c.id == id_user).values(about_me=user_details.about_me).returning(user)
        result = await self._database.fetch_one(stmt)
        if result is None:
            raise HTTPException(status_code=406, detail='Error. About me not change.')
        return {'msg': 'Update info about your.', 'user_id': id_user}


async def get_repository_user():
    return UserRepository(datadb)
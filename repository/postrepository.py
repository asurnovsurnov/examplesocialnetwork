from router import *
from repository.baserepository import BaseRepository

class PostRepository(BaseRepository):

    async def get_posts(self, skip: int = 0, limit: int = 100):
        post = models.Post.__table__
        query = post.select().offset(skip).limit(limit)
        result_list = await self._database.fetch_all(query)
        return [dict(result) for result in result_list]

    async def get_post(self, post_id: str):
        post = models.Post.__table__
        review = models.Review.__table__
        query = post.select().select_from(post.outerjoin(review, post.c.id == review.c.post_id)).where(
            post.c.id == post_id).column(review.c.id).column(review.c.date).column(review.c.rating).column(review.c.from_user)
        try:
            result = await self._database.fetch_all(query)
        except (ValueError, AssertionError):
            raise HTTPException(status_code=400, detail=f'Non-correct data format: {post_id}')
        if result is None: raise HTTPException(status_code=404, detail=f'Dont have post with id: {post_id}')
        result = [dict(el) for el in result]
        answer = {}
        for item in result:
            post_list = answer.setdefault('review', [])
            post_dict = {}
            for key in ['id_1', 'date', 'rating','from_user']:
                post_dict[key] = item.pop(key)
            post_list.append(post_dict)
            answer.update(item)
        if len(answer['review']) == 1 and answer['review'][0]['id_1'] == None: answer['review'] = []
        return answer

    async def get_posts_my(self, user_id: str, skip: int = 0, limit: int = 100):
        post = models.Post.__table__
        review = models.Review.__table__
        query = post.select().select_from(post.outerjoin(review, post.c.id == review.c.post_id)).where(
            post.c.author_id == user_id).order_by(post.c.create_date.desc()).\
            offset(skip).limit(limit).column(review.c.id).column(review.c.rating).column(review.c.from_user)
        result = await self._database.fetch_all(query)
        result = [dict(el) for el in result]
        if not result: return [{'msg':f'You has not posted. Author_id: {user_id}', 'you posts':[]}]
        answer = {}
        for item in result:
            post_list = item.setdefault('review', [])
            post_dict = {}
            for key in ['id_1','rating','from_user']:
                post_dict[key] = item.pop(key)
            post_list.append(post_dict)
            item.update(item)
            if len(item['review']) == 1 and item['review'][0]['id_1'] == None: item['review'] = []
        return result


    async def get_like(self, user_id: str, rating: str, skip: int = 0, limit: int = 100):
        post = models.Post.__table__
        review = models.Review.__table__
        query = post.select().select_from(post.join(review, post.c.id == review.c.post_id)).where(
            (review.c.from_user == user_id) & (review.c.rating == rating)).order_by(review.c.date.desc()).\
            offset(skip).limit(limit).column(review.c.id).column(review.c.date).column(review.c.rating).column(review.c.from_user)
        result = await self._database.fetch_all(query)
        result = [dict(el) for el in result]
        if not result:
            return [{'msg':f'No liked posts found for user with id: {user_id}', 'your likes':[]}]
        #answer = {}
        for item in result:
            post_list = item.setdefault('liked_posts', [])
            post_dict = {}
            for key in ['id_1', 'date', 'rating', 'from_user']:
                post_dict[key] = item.pop(key)
            post_list.append(post_dict)
            item.update(item)
            if len(item['liked_posts']) == 1 and item['liked_posts'][0]['id_1'] == None: item['liked_posts'] = []
        return result

    async def search_posts(self, text: str, skip: int = 0, limit: int = 100):
        post = models.Post.__table__
        query = post.select().where(func.lower(post.c.text).contains(func.lower(text))).offset(skip).limit(limit)
        result_list = await self._database.fetch_all(query)
        if not result_list:
            if (len(text)) < 21: return [{'msg': f'Dont have post with text: {text}'}]
            else: return [{'msg': f'Dont have post with text: {text[:8]}....{text[len(text)-10:len(text)]}'}]
        return [dict(result) for result in result_list]

    async def post_create(self, info_post: shemas.BasePost, name_user: str):
        user = models.SocialUser.__table__
        post = models.Post.__table__
        query_user = user.select().where(user.c.id == name_user)
        id_user = dict(await self._database.fetch_one(query_user))
        info_post.id = uuid.uuid4().hex

        stmt_event = post.insert().values(author_id=id_user['id'], **info_post.dict(exclude_none=True))
        result = await self._database.execute(stmt_event)
        return {'msg': 'Add new Post.', 'id': info_post.id}

    async def post_update(self, info_post: shemas.UpdatePost, name_user: str):
        post = models.Post.__table__
        stmt_post = post.update().where((post.c.id == info_post.id) & (post.c.author_id == name_user)).values(text=info_post.text, update_date=info_post.update_date).returning(post)
        try:
            result = await self._database.fetch_one(stmt_post)
        except (ValueError, AssertionError):
            raise HTTPException(status_code=400, detail=f'Non-correct data format: {info_post.id}')
        if result is None:
            raise HTTPException(status_code=406, detail='This not you"r post')
        return {'msg': 'You update Post.', 'id': info_post.id}

    async def post_delete(self, post_id: str, name_user: str):
        post = models.Post.__table__
        stmt_post = post.delete().where((post.c.id == post_id) & (post.c.author_id == name_user)).returning(post)
        try:
            result = await self._database.fetch_one(stmt_post)
        except (ValueError, AssertionError):
            raise HTTPException(status_code=400, detail=f'Non-correct data format: {post_id}')
        if result is None:
            raise HTTPException(status_code=406, detail='This not your post')
        return {'msg': 'post was delete.', 'id': post_id}

    async def add_like(self, info_like: shemas.BaseReview):
        post = models.Post.__table__
        rew = models.Review.__table__
        query = post.select().where((post.c.id == info_like.post_id) & (post.c.author_id == info_like.from_user))
        try:
            check_author = await self._database.fetch_one(query)
        except (ValueError, AssertionError):
            raise HTTPException(status_code=400, detail=f'Non-correct data format: {info_like.post_id}')
        if check_author is not None:
            raise HTTPException(status_code=406, detail='You cannot like this post because you are the author.')
        like_id = uuid.uuid4().hex
        stmt_post = rew.insert().values(id=like_id, **info_like.dict(exclude_none=True))
        try:
            result = await self._database.execute(stmt_post)
        except UniqueViolationError as e:
            raise HTTPException(status_code=406, detail='You made like this post.')
        return {'msg': 'you add like.', 'id': like_id}

    async def update_like(self, info_like: shemas.BaseReview):
        #user = models.SocialUser.__table__
        post = models.Post.__table__
        rew = models.Review.__table__
        query = post.select().where((post.c.id == info_like.post_id) & (post.c.author_id == info_like.from_user))
        try:
            check_author = await self._database.fetch_one(query)
        except (ValueError, AssertionError):
            raise HTTPException(status_code=400, detail=f'Non-correct data format: {info_like.post_id}')
        if check_author is not None:
            raise HTTPException(status_code=406, detail='You cannot like this post because you are the author.')
        stmt = rew.update().where((rew.c.post_id == info_like.post_id) & (rew.c.from_user == info_like.from_user)).values(**info_like.dict(exclude_none=True)).returning(rew)
        result = await self._database.fetch_one(stmt)
        if result is None:
            raise HTTPException(status_code=406, detail='You"r do not made like in past')
        result = dict(result)
        return {'msg': 'you update like.', 'id': result['id']}

    async def cancel_like(self, post_id, id_user):
        #user = models.SocialUser.__table__
        post = models.Post.__table__
        rew = models.Review.__table__
        query = post.select().where((post.c.id == post_id) & (post.c.author_id == id_user))
        try:
            check_author = await self._database.fetch_one(query)
        except (ValueError, AssertionError):
            raise HTTPException(status_code=400, detail=f'Non-correct data format: {post_id}')
        if check_author is not None:
            raise HTTPException(status_code=406, detail='You cannot like this post because you are the author.')
        stmt = rew.delete().where((rew.c.post_id == post_id) & (rew.c.from_user == id_user)).returning(rew)
        result = await self._database.fetch_one(stmt)
        if result is None:
            raise HTTPException(status_code=404, detail='This not you"r like or post not exist.')
        result = dict(result)
        return {'msg': 'you cancel like.', 'id': result['id']}

    '''
    async def all_post_delete(self, id_user: str): #для разработчика чтобы очищать БД.
        post = models.Post.__table__
        stmt = post.delete().returning(post)
        result_list = await self._database.fetch_all(stmt)
        if result_list is None:
            raise HTTPException(status_code=406, detail='Now is not exists posts')
        #result = [dict(result) for result in result_list]
        return {'msg': 'you delete all posts.'}
    '''

async def get_repository_post():
    return  PostRepository(datadb)
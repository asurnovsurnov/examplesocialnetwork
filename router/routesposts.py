from .__init__ import *

from repository.postrepository import PostRepository, get_repository_post

routepost = APIRouter(prefix="/post", tags=['Post'])


@routepost.get('/read')
@cache(expire=30, namespace='readpost')
async def read_posts(skip: int = 0, limit: int = 100,
                     postrepo: PostRepository = Depends(get_repository_post)) -> list:
    """
    Назначение: \n
    Получить список постов.\n
    """
    result = await postrepo.get_posts(skip=skip, limit=limit)
    return result

@routepost.get('/search')
@cache(expire=30, namespace='searchpost')
async def posts_search(text: str, skip: int = 0, limit: int = 100,
                       postrepo: PostRepository = Depends(get_repository_post)) -> list:
    """
    Назначение: \n
    Получить список постов в результате поиска по тексту.\n
    """
    result = await postrepo.search_posts(text=text, skip=skip, limit=limit)
    return result


@routepost.get('/{post_id}/read')
@cache(expire=10, namespace='readpost')
async def read_post(post_id: str, postrepo: PostRepository = Depends(get_repository_post)) -> dict:
    """
    Назначение: \n
    Получить пост по идентифкатору. \n
    В случае когда формат идентификатора не верен, возникает исключение HTTPException с кодом состояния 400. \n
    Если идентификатор поста не найден, возникает исключение HTTPException с кодом состояния 404.
    """
    result = await postrepo.get_post(post_id=post_id)
    return result


@routepost.get('/read/likes')
@cache(expire=30, namespace='readpost')                             # кэширование постов которым пользователь дал оценку.
async def read_likes(rating: ReviewRating,
                        skip: int = 0,
                        limit: int = 100,
                        postrepo: PostRepository = Depends(get_repository_post),
                        credentials: HTTPAuthorizationCredentials = Security(security)) -> list:
    """
    Назначение: \n
    Получить список постов которым пользователь поставил оценку.\n
    Если предоставленные учетные данные пользователя недействительны, возникает исключение HTTPException с кодом состояния 401.
    """
    id_user = auth_handler.decode_token(credentials.credentials)
    result = await postrepo.get_like(user_id=id_user, rating=rating, skip=skip, limit=limit)
    return result


@routepost.get('/read/my')
@cache(expire=30, namespace='readpost')
async def read_my_posts(skip: int = 0,
                        limit: int = 100,
                        postrepo: PostRepository = Depends(get_repository_post),
                        credentials: HTTPAuthorizationCredentials = Security(security)) -> list:
    """
    Назначение: \n
    Получить список cвоих постов.\n
    Если предоставленные учетные данные пользователя недействительны, возникает исключение HTTPException с кодом состояния 401.
    """
    id_user = auth_handler.decode_token(credentials.credentials)
    result = await postrepo.get_posts_my(user_id=id_user, skip=skip, limit=limit)
    return result


@routepost.post('/create')
async def create_post(info_post: shemas.BasePost,
                       postrepo: PostRepository = Depends(get_repository_post),
                       credentials: HTTPAuthorizationCredentials = Security(security)):
    """
    Назначение: \n
    Создать новый пост, получая сведения в теле запроса. \n
    Если предоставленные учетные данные пользователя недействительны, возникает исключение HTTPException с кодом состояния 401. \n
    Идентификатор поста генерируется автоматически во время создания.
    """
    id_user = auth_handler.decode_token(credentials.credentials)
    if isinstance(info_post.create_date, datetime):
        dt_string = info_post.create_date.strftime("%Y-%m-%d %H:%M:%S")
        info_post.create_date = datetime.strptime(dt_string, "%Y-%m-%d %H:%M:%S")
    if info_post.create_date is None:
        info_post.create_date = datetime.strptime(datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "%Y-%m-%d %H:%M:%S")
    return await postrepo.post_create(info_post=info_post, name_user=id_user)


@routepost.put('/{post_id}/update/')
async def update_post(post_id: str,
                      info_post: shemas.UpdatePost,
                      postrepo: PostRepository = Depends(get_repository_post),
                      credentials: HTTPAuthorizationCredentials = Security(security)):
    """
    Назначение: \n
    Обновить существующий пост, получая новые данные в теле запроса. \n
    В случае когда пользователь не является автором поста, возникает исключение HTTPException с кодом состояния 406. \n
    Если предоставленные учетные данные пользователя недействительны, возникает исключение HTTPException с кодом состояния 401. \n
    При неверном формате идентификатора, возникает исключение HTTPException с кодом состояния 400. \n
    Параметр идентификатора поста всегда принимается из адресной строки запроса.
    """
    id_user = auth_handler.decode_token(credentials.credentials)
    info_post.id = post_id
    if isinstance(info_post.update_date, datetime):
        dt_string = info_post.update_date.strftime("%Y-%m-%d %H:%M:%S")
        info_post.update_date = datetime.strptime(dt_string, "%Y-%m-%d %H:%M:%S")
    if info_post.update_date is None:
        info_post.update_date = datetime.strptime(datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "%Y-%m-%d %H:%M:%S")
    return await postrepo.post_update(info_post=info_post, name_user=id_user)


@routepost.delete('/{post_id}/delete/')
async def delete_post(post_id: str,
                      postrepo: PostRepository = Depends(get_repository_post),
                      credentials: HTTPAuthorizationCredentials = Security(security)):
    """
    Назначение: \n
    Удалить существующий пост. \n
    В случае когда пользователь не является автором поста, возникает исключение HTTPException с кодом состояния 406. \n
    Если предоставленные учетные данные пользователя недействительны, возникает исключение HTTPException с кодом состояния 401. \n
    При неверном формате идентификатора, возникает исключение HTTPException с кодом состояния 400.
    """
    id_user = auth_handler.decode_token(credentials.credentials)
    return await postrepo.post_delete(post_id=post_id, name_user=id_user)


@routepost.post('/{post_id}/like')
async def like_add(post_id: str,
                   rating: ReviewRating,
                   info_like: shemas.BaseReview,
                   postrepo: PostRepository = Depends(get_repository_post),
                   credentials: HTTPAuthorizationCredentials = Security(security)):
    """
    Назначение: \n
    Поставить оценку посту.\n
    В случае когда пользователь является автором поста, возникает исключение HTTPException с кодом состояния 406. \n
    Если предоставленные учетные данные пользователя недействительны, возникает исключение HTTPException с кодом состояния 401. \n
    При неверном формате идентификатора, возникает исключение HTTPException с кодом состояния 400. \n
    Параметр оценки реакции и идентификатора поста всегда принимается из адресной строки.
    """
    id_user = auth_handler.decode_token(credentials.credentials)
    # возможность получить недостоющие данные непосредственно на сервере в случае ошибки сообщения.
    if isinstance(info_like.date, datetime):
        dt_string = info_like.date.strftime("%Y-%m-%d %H:%M:%S")
        info_like.date = datetime.strptime(dt_string, "%Y-%m-%d %H:%M:%S")
    for key, value in vars(info_like).items():
        if key == 'from_user': info_like.from_user = id_user
        elif key == 'post_id': info_like.post_id = post_id
        elif key == 'rating': info_like.rating = rating
        elif key == 'date': info_like.date = datetime.strptime(datetime.now().strftime("%Y-%m-%d %H:%M:%S"),"%Y-%m-%d %H:%M:%S") if value is None else value
    return await postrepo.add_like(info_like = info_like)


@routepost.put('/{post_id}/like')
async def like_update(post_id: str,
                      rating: ReviewRating,
                      info_like: shemas.BaseReview,
                      postrepo: PostRepository = Depends(get_repository_post),
                      credentials: HTTPAuthorizationCredentials = Security(security)):
    """
    Назначение: \n
    Изменить оценку поста.\n
    В случае когда пользователь является автором поста, возникает исключение HTTPException с кодом состояния 406. \n
    Если предоставленные учетные данные пользователя недействительны, возникает исключение HTTPException с кодом состояния 401. \n
    При неверном формате идентификатора, возникает исключение HTTPException с кодом состояния 400. \n
    Параметр оценки реакции и идентификатора поста всегда принимается из адресной строки.
    """

    id_user = auth_handler.decode_token(credentials.credentials)
    # возможность получить недостоющие данные непосредственно на сервере в случае ошибки сообщения.
    if isinstance(info_like.date, datetime):
        dt_string = info_like.date.strftime("%Y-%m-%d %H:%M:%S")
        info_like.date = datetime.strptime(dt_string, "%Y-%m-%d %H:%M:%S")
    for key, value in vars(info_like).items():
        if key == 'from_user': info_like.from_user = id_user
        elif key == 'post_id': info_like.post_id = post_id
        elif key == 'rating': info_like.rating = rating #if value is None else value
        elif key == 'date': info_like.date = datetime.strptime(datetime.now().strftime("%Y-%m-%d %H:%M:%S"),"%Y-%m-%d %H:%M:%S") if value is None else value
    return await postrepo.update_like(info_like = info_like)


@routepost.delete('/{post_id}/like')
async def like_cancel(post_id: str,
                      postrepo: PostRepository = Depends(get_repository_post),
                      credentials: HTTPAuthorizationCredentials = Security(security)):
    """
    Назначение: \n
    Отменить оценку поста.\n
    В случае когда пользователь является автором поста, возникает исключение HTTPException с кодом состояния 406. \n
    Если предоставленные учетные данные пользователя недействительны, возникает исключение HTTPException с кодом состояния 401. \n
    При неверном формате идентификатора, возникает исключение HTTPException с кодом состояния 400. \n
    """

    id_user = auth_handler.decode_token(credentials.credentials)
    return await postrepo.cancel_like(post_id=post_id, id_user=id_user)







'''
@routepost.delete('/all')
async def all_posts_delete(postrepo: PostRepository = Depends(get_repository_post),
                           credentials: HTTPAuthorizationCredentials = Security(security)):
    """
    Назначение: \n
    Удалить все посты (Метод для разработчика, чтобы чистить БД).\n
    В случае когда пользователь является автором поста, возникает исключение HTTPException с кодом состояния 406. \n
    Если предоставленные учетные данные пользователя недействительны, возникает исключение HTTPException с кодом состояния 401.
    """

    id_user = auth_handler.decode_token(credentials.credentials)
    return await postrepo.all_post_delete(id_user=id_user)
'''
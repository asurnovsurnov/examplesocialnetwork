from fastapi import APIRouter, Depends
from fastapi_cache.decorator import cache
from fastapi import FastAPI, HTTPException, Path, Security
from fastapi.responses import FileResponse, StreamingResponse
from fastapi.security import HTTPAuthorizationCredentials, HTTPBearer
import random
import string
from asyncpg.exceptions import UniqueViolationError
from sqlalchemy.exc import InvalidRequestError
from sqlalchemy import func
from dbcon.database import engine, datadb
from dbcon import models, shemas
from dbcon.shemas import *
import io
import base64
import auth
import uuid


security = HTTPBearer()

auth_handler = auth.Auth()




'''
from .__init__ import *

class BaseRepository:
    def __init__(self, database: datadb):
        self._database = datadb
'''
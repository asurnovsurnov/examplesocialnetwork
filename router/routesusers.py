import time
from .__init__ import *
from repository.userrepository import UserRepository, get_repository_user


routeuser = APIRouter(prefix="/user", tags=['User'])


@routeuser.get('/read')
@cache(expire=30, namespace='readuser')
async def read_users(skip: int = 0, limit: int = 100, repouser: UserRepository = Depends(get_repository_user)) -> list:
    """
    Назначение: \n
    Получить список пользователей.\n
    """
    #time.sleep(5) #для проверки кэширования
    return await repouser.get_users(skip=skip, limit=limit)


@routeuser.get('/{user_id}/read')
@cache(expire=10, namespace='readuser')
async def read_user(user_id: str, repouser: UserRepository = Depends(get_repository_user)) -> dict:
    """
    Назначение: \n
    Получить информацию о пользователе из базы данных по идентифкатору. \n
    В случае когда формат идентификатора не верен, возникает исключение HTTPException с кодом состояния 400. \n
    Если идентификатор пользователя не найден, возникает исключение HTTPException с кодом состояния 404.
    """
    return await repouser.get_user(user_id=user_id)

@routeuser.get('/search')
@cache(expire=30, namespace='searchuser')
async def users_search(search_string: str, how_search: SearchUser, skip: int = 0, limit: int = 100,
                       repouser: UserRepository = Depends(get_repository_user)) -> list:
    """
    Назначение: \n
    Найти и получить список пользователей из параметров поиска.\n
    """
    #time.sleep(5) #для проверки кэширования

    return await repouser.search_users(search_string=search_string, how_search=how_search, skip=skip, limit=limit)


@routeuser.post('/register')
async def make_register(user_details: shemas.BaseUser, repouser: UserRepository = Depends(get_repository_user)) -> dict:
    """
    Назначение: \n
    Зарегистрировать нового пользователя в системе, получая данные в теле запроса. \n
    Идентификатор пользователя генерируется автоматически во время создания.
    """
    if isinstance(user_details.register_date, datetime):
        dt_string = user_details.register_date.strftime("%Y-%m-%d %H:%M:%S")
        user_details.register_date = datetime.strptime(dt_string, "%Y-%m-%d %H:%M:%S")
    elif user_details.register_date is None:
        user_details.register_date = datetime.strptime(datetime.now().strftime("%Y-%m-%d %H:%M:%S"),"%Y-%m-%d %H:%M:%S")
    return await repouser.make_singup_user(user_details=user_details)


@routeuser.post('/login')
async def login(user_details: shemas.UserAuth, repouser: UserRepository = Depends(get_repository_user)) -> dict:
    """
    Назначение: \n
    Вход в социальную сеть.\n
    Аутентифицировать существующего пользователя в системе, получая детали в теле запроса. \n
    Если предоставленные учетные данные недействительны, возникает исключение HTTPException с кодом состояния 401.
    """
    return await repouser.make_login_user(user_details=user_details)


@routeuser.put('/change/password')
async def change_password(user_details: shemas.UserPasswordChange,
                          repouser: UserRepository = Depends(get_repository_user),
                          credentials: HTTPAuthorizationCredentials = Security(security)) -> dict:
    """
    Назначение: \n
    Изменить пароль пользователя на интернет-платформе.\n
    В случае ошибки транзакции, возникает исключение HTTPException с кодом состояния 406. \n
    Если предоставленные учетные данные недействительны или запрашивающая сторона не является существующим пользователем, возникает исключение HTTPException с кодом состояния 401.
    """
    user_id = auth_handler.decode_token(credentials.credentials)
    user_details.id = user_id
    return await repouser.password_change_user(user_details=user_details)


@routeuser.put('/change/name')
async def change_name(user_details: shemas.UserNameChange,
                          repouser: UserRepository = Depends(get_repository_user),
                          credentials: HTTPAuthorizationCredentials = Security(security)) -> dict:
    """
    Назначение: \n
    Изменить имя пользователя на интернет-платформе.\n
    В случае ошибки транзакции, возникает исключение HTTPException с кодом состояния 406. \n
    Если предоставленные учетные данные недействительны или запрашивающая сторона не является существующим пользователем, возникает исключение HTTPException с кодом состояния 401.
    """
    user_id = auth_handler.decode_token(credentials.credentials)
    return await repouser.name_change_user(user_details=user_details, id_user=user_id)

@routeuser.put('/change/about')
async def change_aboutinfo(user_details: shemas.UserAboutChange,
                          repouser: UserRepository = Depends(get_repository_user),
                          credentials: HTTPAuthorizationCredentials = Security(security)) -> dict:
    """
    Назначение: \n
    Изменить информацию о пользователе на интернет-платформе.\n
    В случае ошибки транзакции, возникает исключение HTTPException с кодом состояния 406. \n
    Если предоставленные учетные данные недействительны или запрашивающая сторона не является существующим пользователем, возникает исключение HTTPException с кодом состояния 401.
    """
    user_id = auth_handler.decode_token(credentials.credentials)
    return await repouser.about_change_user(user_details=user_details, id_user=user_id)

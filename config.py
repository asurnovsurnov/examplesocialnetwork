from dotenv import dotenv_values


info_env = dotenv_values('.env')


POSTGRES_USER = info_env.get('POSTGRES_USER')
POSTGRES_PASSWORD = info_env.get('POSTGRES_PASSWORD')
POSTGRES_DB = info_env.get('POSTGRES_DB')
POSTGRES_HOST = info_env.get('POSTGRES_HOST')
POSTGRES_PORT = info_env.get('POSTGRES_PORT')
APP_SECRET_STRING = info_env.get('APP_SECRET_STRING')
ALGORITM_USER = info_env.get('ALGORITM_USER')
#APP_ADMIN_SECRET_STRING = info_env.get('APP_ADMIN_SECRET_STRING')


import asyncpg
from .database import Base, engine, POSTGRES_USER, POSTGRES_PASSWORD, POSTGRES_HOST, POSTGRES_PORT, POSTGRES_DB
from .models import *


async def create_database_if_not_exists():
    connection_url = f'postgresql://{POSTGRES_USER}:{POSTGRES_PASSWORD}@{POSTGRES_HOST}:{POSTGRES_PORT}/postgres'
    pool = await asyncpg.create_pool(connection_url)
    async with pool.acquire() as connection:
        await connection.execute(f'CREATE DATABASE {POSTGRES_DB} OWNER {POSTGRES_USER}')


async def create_shema_for_database():
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)

import os
import asyncpg
from databases import Database

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.ext.asyncio import create_async_engine
from config import POSTGRES_DB, POSTGRES_PASSWORD, POSTGRES_HOST, POSTGRES_PORT, POSTGRES_USER


engine = create_async_engine('postgresql+asyncpg://{user}:{passwd}@{host}:{port}/{db}'.format(user=POSTGRES_USER, passwd=POSTGRES_PASSWORD, host=POSTGRES_HOST, port=5432, db=POSTGRES_DB), pool_size=8)

datadb = Database('postgresql+asyncpg://{user}:{passwd}@{host}:{port}/{db}'.format(user=POSTGRES_USER, passwd=POSTGRES_PASSWORD, host=POSTGRES_HOST, port=5432, db=POSTGRES_DB))

Base = declarative_base()

async def check_database_exists():
    # Устанавливаем соединение с PostgreSQL
    try:
        conn = await asyncpg.connect(host=POSTGRES_HOST, port=POSTGRES_PORT, user=POSTGRES_USER, password=POSTGRES_PASSWORD, database=POSTGRES_DB)
        # Проверяем существование базы данных.
        db_exists = await conn.fetchval("SELECT 1 FROM pg_database WHERE datname=$1", POSTGRES_DB)
        return True
    except asyncpg.exceptions.InvalidCatalogNameError:
        return False


db_session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=engine, class_=AsyncSession))

Base.query = db_session.query_property()
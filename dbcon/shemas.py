import re
from datetime import datetime, date
from typing import Optional, List, Union
from pydantic import BaseModel, EmailStr, Field, validator
from enum import Enum

class ReviewRating(str, Enum):
    positive = 'positive'
    negative = 'negative'


class SearchUser(str, Enum):
    all = 'all'
    name = 'name'

class BaseUser(BaseModel):
    id: Optional[str] = Field(default=None, exclude=True)
    first_name: str
    last_name: str
    email: str
    password: str
    about_me: Optional[str] = None
    register_date: Optional[datetime] = Field(default=None)

    @validator("email")
    @classmethod
    def validate_email(cls, value):
        if not bool(re.fullmatch(r'[\w.-]+@[\w-]+\.[\w.]+', value)):
            raise ValueError("Email is invalid")
        return value

    @validator("password")
    @classmethod
    def password_validator(cls, value):
        if len(value) <= 8 or len(value) > 21:
            raise ValueError("Password must be between 8 and 20 characters")
        if not re.search(r'\d', value) or not re.search(r'[a-zA-Z]', value):
            raise ValueError("Password must contain at least one digit and one letter")
        return value

    @validator("first_name")
    @classmethod
    def first_name_validator(cls, value):
        if len(value) <= 1 or len(value) > 35:
            raise ValueError("First name must be between 1 and 35 characters")
        if re.search(r'\d', value) or not re.search(r'[a-zA-Zа-яА-Я]', value):
            raise ValueError("First name must be contain only letters")
        return value

    @validator("last_name")
    @classmethod
    def last_name_validator(cls, value):
        if len(value) <= 1 or len(value) > 35:
            raise ValueError("Last name must be between 1 and 35 characters")
        if re.search(r'\d', value) or not re.search(r'[a-zA-Zа-яА-Я]', value):
            raise ValueError("Last name must be contain only letters")
        return value

    @validator("about_me")
    @classmethod
    def about_me_validator(cls, value):
        if len(value) <= 12 or len(value) > 1800:
            raise ValueError("Information of you must be between 12 and 1800 characters")
        return value

    @validator('register_date')
    @classmethod
    def register_date_validator(cls, value):
        if value is not None:
            try:
                datetime.strptime(value.strftime('%Y-%m-%d %H:%M:%S'), '%Y-%m-%d %H:%M:%S')
            except ValueError:
                raise ValueError("Incorrect datetime format, should be 'YYYY-MM-DD HH:MM:SS'")
        return value

class UserAuth(BaseUser):
    id: Optional[str] = Field(default=None, exclude=True)
    first_name: Optional[str] = Field(default=None, exclude=True)
    last_name: Optional[str] = Field(default=None, exclude=True)
    about_me: Optional[str] = Field(default=None, exclude=True)
    register_date: Optional[datetime] = Field(default=None, exclude=True)
    email: str
    password: str


class UserPasswordChange(UserAuth):
    id: Optional[str] = Field(default=None, exclude=True)
    email: str
    password: str
    new_password: str

    @validator('new_password')
    @classmethod
    def new_password_validator(cls, value):
        if len(value) <= 8 or len(value) > 21:
            raise ValueError("Password must be between 8 and 20 characters")
        if not re.search(r'\d', value) or not re.search(r'[a-zA-Z]', value):
            raise ValueError("Password must contain at least one digit and one letter")
        return value

class UserNameChange(UserAuth):
    id: Optional[str] = Field(default=None, exclude=True)
    email: Optional[str] = Field(default=None, exclude=True)
    password: Optional[str] = Field(default=None, exclude=True)
    first_name: str
    last_name: str


class UserAboutChange(UserNameChange):
    first_name: Optional[str] = Field(default=None, exclude=True)
    last_name: Optional[str] = Field(default=None, exclude=True)
    about_me: str


class BasePost(BaseModel):
    id: Optional[str] = None
    create_date: Optional[datetime] = Field(default=None)
    text: str

    @validator('text')
    @classmethod
    def text_validator(cls, value):
        if len(value) <= 10 or len(value) > 5300:
            raise ValueError("Message must be between 10 and 5300 characters")
        return value

    @validator('create_date')
    @classmethod
    def create_date_validator(cls, value):
        if value is not None:
            try:
                datetime.strptime(value.strftime('%Y-%m-%d %H:%M:%S'), '%Y-%m-%d %H:%M:%S')
            except ValueError:
                raise ValueError("Incorrect datetime format, should be 'YYYY-MM-DD HH:MM:SS'")
        return value


class UpdatePost(BasePost):
    id: Optional[str] = None
    create_date: Optional[datetime] = Field(default=None, exclude=True)
    update_date: Optional[datetime] = Field(default=None)
    text: str

    @validator('update_date')
    @classmethod
    def update_date_validator(cls, value):
        if value is not None:
            try:
                datetime.strptime(value.strftime('%Y-%m-%d %H:%M:%S'), '%Y-%m-%d %H:%M:%S')
            except ValueError:
                raise ValueError("Incorrect datetime format, should be 'YYYY-MM-DD HH:MM:SS'")
        return value

class BaseReview(BaseModel):
    #id: Optional[str] = None
    from_user: Optional[str] = None
    rating: Optional[str] = None
    date: Optional[datetime] = Field(default=None)
    post_id: Optional[str] = None

    @validator('date')
    @classmethod
    def date_validator(cls, value):
        if value is not None:
            try:
                datetime.strptime(value.strftime('%Y-%m-%d %H:%M:%S'), '%Y-%m-%d %H:%M:%S')
            except ValueError:
                raise ValueError("Incorrect datetime format, should be 'YYYY-MM-DD HH:MM:SS'")
        return value

    @validator('rating')
    @classmethod
    def rating_validator(cls, value):
        if (value is not None) and (value not in ["positive", "negative"]):
            raise ValueError("Value must be 'positive' or 'negative'")
        return value


class PostOuth(BaseModel):
    id: Optional[str] = None
    text: Optional[str] = None


class PostList(BaseModel):
    __root__: List[PostOuth] = []
from sqlalchemy.orm import relationship, backref
from datetime import *
from sqlalchemy import inspect

from sqlalchemy_utils import UUIDType
from sqlalchemy.exc import InvalidRequestError
import uuid
from datetime import timedelta
from passlib.hash import bcrypt
from dbcon.database import Base
import sqlalchemy as db



class SocialUser(Base):
    __tablename__ = 'socialusers'
    id = db.Column(UUIDType(binary=False), primary_key=True)
    first_name = db.Column(db.String(250), nullable=False)
    last_name = db.Column(db.String(250), nullable=False)
    email = db.Column(db.String(250), nullable=False, unique=True)
    password = db.Column(db.String(250), nullable=False)
    about_me = db.Column(db.String(1800))
    register_date = db.Column(db.DateTime, nullable=False)
    post_id = relationship('Post', backref='socialposts', lazy='joined')
    my_review = relationship('Review', backref='reviews', lazy='joined')


class Post(Base):
    __tablename__ = 'socialposts'
    id = db.Column(UUIDType(binary=False), primary_key=True)
    create_date = db.Column(db.DateTime, nullable=False)
    update_date = db.Column(db.DateTime)
    text = db.Column(db.String(5300), nullable=False)
    author_id = db.Column(UUIDType(binary=False), db.ForeignKey('socialusers.id', ondelete='CASCADE'), nullable=False)
    review_id = relationship('Review', backref='reviews', lazy='joined')


class Review(Base):
    __tablename__ = 'reviews'
    id = db.Column(UUIDType(binary=False), primary_key=True)
    from_user = db.Column(UUIDType(binary=False), db.ForeignKey('socialusers.id', ondelete='CASCADE'), nullable=False)
    rating = db.Column(db.String(150), db.CheckConstraint("rating IN ('positive', 'negative')"), nullable=False)
    date = db.Column(db.DateTime, nullable=False)
    post_id = db.Column(UUIDType(binary=False), db.ForeignKey('socialposts.id', ondelete='CASCADE'), nullable=False)
    __table_args__ = (db.UniqueConstraint('from_user', 'post_id', name='_user_post_uc'),)
